<?php

class Population implements Countable {
    private $population = array();
    private $next_generation;
    public function count() {
        return $this->size;
    }
    
    public function __construct($size=0) {
        $this->size = $size;
        $this->elite_count = 0;
    }
    
    public function setGenome($genome) {
        $this->genome = $genome;
    }
    
    public function getPopulation() {
        if (count($this->population) == 0) {
            $this->generate();
        }
        return $this->population;
    }
    
    public function generate() {
        $this->population = array();
        for($i=0; $i<$this->size; ++$i) {
            $this->population[]= $this->genome->createIndividual();
        }
    }
    
    public function setEliteCount($count) {
        $this->elite_count = $count;
    }
    
    public function evolve() {
        $this->next_generation = new Population();
        $this->next_generation->setGenome($this->genome);
        $this->next_generation->setEliteCount($this->elite_count);
        
        $this->preserve_elite();
        
        $population_pool = $this->prepare_choice_wheel();
        
        $offspring_size = $this->size - $this->elite_count;
        for($key=0; $key < $offspring_size; ++$key) {
            $child = $this->genome->mate(
                $population_pool[$key],
                $population_pool[$key+1]
            );
            $this->genome->mutate($child);
            $this->next_generation->add(
                $child
            );
        }
        
        return $this->next_generation;
    }
    
    private function preserve_elite() {
        $elite = $this->genome->getElite($this->population, $this->elite_count);
        foreach ($elite as $elite_member) {
            $this->next_generation->add($elite_member);
        }
    }
    
    private function prepare_choice_wheel() {
        $population_pool = array();
        foreach($this->population as $id) {
            $id_fitness = $this->genome->evaluate($id);
            for($i=0; $i < $id_fitness*2; ++$i) {
                $population_pool[] = $id;
            }
        }
        shuffle($population_pool);
        return array_slice($population_pool, 0, $this->size*2);
    }
    
    public function add($id) {
        ++$this->size;
        $this->population[] = $id;
    }
    
    public function calculateFitness() {
        $score = 0;
        foreach($this->population as $id) {
            $score += $this->genome->evaluate($id);
        }
        return $score;
    }
    
    public function bestIndividual() {
        $score = 0;
        $index = 0;
        foreach($this->population as $key=>$id) {
            $id_score = $this->genome->evaluate($id);
            if ($id_score > $score) {
                $score = $id_score;
                $index = $key;
            }
        }
        return $this->population[$index];
    }
}
