<?php
require_once 'src'.DIRECTORY_SEPARATOR.'Genome.php';
require_once 'src'.DIRECTORY_SEPARATOR.'Population.php';

$cinema_map = array(
    0,0,0,0,0,0,
    1,1,1,1,1,1,
    2,2,2,2,2,2,
    1,1,1,1,1,1,
    0,0,0,0,0,0
);

function evaluation($chromosome) {
    global $cinema_map;

   $score = 0;
   if (count(array_unique($chromosome)) < 3) {
       return 0;
   }
   foreach($chromosome as $nuke)  {
       $score += $cinema_map[$nuke];
   }
   if ((max($chromosome)-min($chromosome) == 2) 
     &&(max($chromosome)%6 - min($chromosome)%6==2)) {
       $score += 3;
   }
   return $score;
}

$genome = new Genome(3, 0, 29);
$genome->setEvaluator("evaluation");
$genome->setMutationRate(0.01);
$genome->setCrossoverRate(0.80);
$population = new Population(100);
$population->setGenome($genome);
$population->setEliteCount(1);

$population->generate();

for($i=0; $i<50; ++$i) {
    $population = $population->evolve();
    
}

print_r($population->bestIndividual());