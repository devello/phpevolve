<?php

class Genome {
    
    public function __construct($size = 3, $min_value = 0, $max_value=10) {
        $this->size = $size;
        $this->min_value = $min_value;
        $this->max_value = $max_value;
        $this->evaluator = create_function('$gene', 'return 0;');
        $this->mutation_rate = 0.01;
        $this->crossover_rate = 0.75;
    }
    
    public function createIndividual() {
        $individual = array();
        $choice = range($this->min_value, $this->max_value);
        
        for ($i=0; $i< $this->size; ++$i) {
            shuffle($choice);
            $individual[] = $choice[$i];
        }
        return $individual;
    }
    public function mutate(&$gene) {
        if ($this->shouldNotMutate()) {
            return $gene;
        }
        $rand = rand(0, $this->size-1);
        $gene[$rand] = rand($this->min_value, $this->max_value - 1);
        return $gene;
    }
    
    private function shouldNotMutate() {
        $rand = rand(0, 100) / 100;
        if ($rand <= $this->mutation_rate) {
            return false;
        } 
        return true;
    }
    
    public function setMutationRate($rate) {
        $this->mutation_rate = $rate;
    }
    
    public function setCrossoverRate($rate) {
        $this->crossover_rate = $rate;
    }
    
    public function mate($gene1, $gene2) {
        if ($this->crossover_rate <=0.0) {
            return $gene1;
        }
        if ((rand(0,100) / 100) > $this->crossover_rate) {
            return $gene1;
        }
        $pivot_point = rand(0, $this->size-1);
        $child = array();
        for ($i=0; $i<$pivot_point; ++$i) {
            $child[]=$gene1[$i];
        }
        for ($i=$pivot_point; $i<$this->size; ++$i) {
            $child[]=$gene2[$i];
        }
        return $child;
    }
    
    public function getElite($raw_population, $limit) {
        uasort($raw_population, array($this, "cmp"));
        $elite = array();
        $population_size = count($raw_population);
        for($i=0; $i < $limit && $i < $population_size; ++$i) {
            $elite[] = array_pop($raw_population);
        }
        return $elite;
    }
    
    public function evaluate($gene) {
        return call_user_func(
            $this->evaluator,
            $gene);
    }
    
    public function cmp($id1, $id2) {
        $score_id1 = $this->evaluate($id1);
        $score_id2 = $this->evaluate($id2);
        if ($score_id1 > $score_id2) {
            return 1;
        }
        if ($score_id2 > $score_id1) {
            return -1;
        }
        return 0;
    }
    
    public function setEvaluator($callback) {
        $this->evaluator = $callback;
    }
}
