<?php

require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Genome.php';
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Population.php';
function dummy_fitness($gene) {
    return 5;
}

function example_fitness2($gene) {
    $score = 0;
    foreach ($gene as $nuke) {
        $score += $nuke;
        
    }
    return $score;
}

class PopulationTest extends PHPUnit_Framework_TestCase {
    
    public function test_population_has_size() {
        $p = new Population();
        $this->assertEquals(0, count($p));
    }
    
    public function test_population_is_generated_upon_construction () {
        $p = new Population(2);
        $p->setGenome(new Genome(3, 0, 10));
        $this->assertEquals(2, count($p->getPopulation()));
    }
    
    public function test_individuals_in_the_population_are_of_give_genome_type() {
        $p = new Population(3);
        $p->setGenome(new Genome(3, 5, 10));
        $p->generate();
        
        $this->assertEquals(3, count($p));
        $this->assertEquals(3, count($p->getPopulation()));
        
        foreach($p->getPopulation() as $id) {
            $this->assertEquals(3, count($id));
        }
    }
    
    public function test_population_can_evolve() {
        $p = new Population(3);
        $g = new Genome(3, 5, 10);
        $g->setEvaluator("example_fitness2");
        $p->setGenome($g);
        $p->generate();
        
        $p = $p->evolve();
        
        $this->assertEquals(3, count($p));
    }
    
    public function test_an_individual_can_be_added_to_population() {
        $p = new Population(3);
        $g = new Genome(3, 5, 10);
        $p->setGenome(new Genome(3, 5, 10));
        $p->generate();
        
        $this->assertEquals(3, count($p));
        $p->add($g->createIndividual());
        
        $this->assertEquals(4, count($p));
        $this->assertEquals(4, count($p->getPopulation()));
        
        foreach($p->getPopulation() as $id) {
            $this->assertEquals(3, count($id));
        }
    }
    
    public function test_population_statistics_can_calculate_average_fitness() {
        srand(0);
        $p = new Population();
        
        $g = new Genome(3);
        $g->setEvaluator("example_fitness2");
        
        $p->setGenome($g);
        $p->add(array(1,2,3));
        $p->add(array(0,1,10));
        $p->add(array(10,10,10));
        //$p->generate();
        
        $this->assertEquals(47, $p->calculateFitness(), "", 0.1);
    }
    
    public function test_population_average_fitness_improves_after_evolve() {
        $p = new Population(3);
        
        $g = new Genome(3, 5, 10);
        $g->setEvaluator("example_fitness2");
        
        $p->setGenome($g);
        $p->generate();
        
        $original_fitness = $p->calculateFitness();
        $p = $p->evolve();
        $improved_fitness = $p->calculateFitness();
        
        $this->assertTrue($improved_fitness > $original_fitness);
    }
    
    public function test_population_can_point_its_best_individual() {
        srand(0);
        $p = new Population();
        $g = new Genome(3, 5, 10);
        $g->setEvaluator("example_fitness2");
        $p->setGenome($g);
        $p->add(array(5,5,5));
        $p->add(array(6,9,9));
        $p->add(array(5,6,5));
        $p->add(array(7,5,6));
        $this->assertEquals(array(6,9,9), $p->bestIndividual());
    }
    
    public function test_best_individual_survives_when_elitarism_is_enabled() {
        
        $p = new Population();
        $g = new Genome(2, 0, 10);
        $g->setCrossoverRate(1.0);
        $g->setMutationRate(1.0);
        $g->setEvaluator("example_fitness2");
        $p->setGenome($g);
        $p->add(array(1,1));
        $p->add(array(9,9));
        
        $best_individual = array(9,9);
        $p->setEliteCount(1);
        $p = $p->evolve();
        
        $this->assertTrue(in_array($best_individual, $p->getPopulation()));
    }
    
    public function test_declared_best_individuals_count_can_be_preserved() {
        $p = new Population();
        
        $g = new Genome(2, 0, 10);
        $g->setCrossoverRate(1.0);
        $g->setMutationRate(1.0);
        $g->setEvaluator("example_fitness2");
        
        $p->setGenome($g);
        $p->add(array(1,1));
        $p->add(array(2,2));
        
        $p->add(array(8,8));
        $p->add(array(9,9));
        
        $p->setEliteCount(2);
        
        $p = $p->evolve();
        
        $this->assertTrue(in_array(array(8,8), $p->getPopulation()));
        $this->assertTrue(in_array(array(9,9), $p->getPopulation()));
        
    }
}
