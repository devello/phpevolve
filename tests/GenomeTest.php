<?php
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Genome.php';
function example_fitness($gene) {
    $score = 0;
    foreach ($gene as $nuke) {
        $score += $nuke;
        
    }
    return $score;
}
class GenomeTest extends PHPUnit_Framework_TestCase {
    
    public function setUp() {
        srand(0);
    }
    public function test_genome_can_be_instatiated() {
        $g = new Genome();
    }
    
    public function test_genome_can_produce_an_invidual() {
        $g = new Genome();
        $i = $g->createIndividual();
        
    }
    
    public function test_an_individual_has_size() {
        $g = new Genome(3);
        $i = $g->createIndividual();
        
        $this->assertEquals(3, count($i));
    }
    
    public function test_an_individual_length_can_be_arbitrary() {
        $g = new Genome(5);
        $i = $g->createIndividual();
        $this->assertEquals(5, count($i));
    }
    
    public function test_genome_alphabet_can_be_from_min_values_to_max_values() {
        $g = new Genome(5, 5, 10);
        $i = $g->createIndividual();
        foreach ($i as $code_value) {
            $this->assertTrue($code_value >= 5 && $code_value <= 10, "$code_value");
        }
    }
    
    public function test_genome_can_mutate_an_individual() {
        $g = new Genome(5, 5, 10);
        $g->setMutationRate(1.0);
        $i = $g->createIndividual();
        $copy = $i;
        $g->mutate($i);
        $this->assertNotEquals($i, $copy);
    }
    
    public function test_mutation_can_be_turned_off() {
        $g = new Genome(5, 5, 10);
        $g->setMutationRate(0.0);
        $i = $g->createIndividual();
        $copy = $i;
        $g->mutate($i);
        $this->assertEquals($i, $copy);
        
    }
    
    public function test_gename_can_mate_individuals() {
        $g = new Genome(5, 5, 10);
        $g->setCrossoverRate(1.0);
        $id1 = $g->createIndividual();
        $id2 = $g->createIndividual();
        
        $id3 = $g->mate($id1, $id2);
        $this->assertNotEquals($id3, $id1);
        $this->assertNotEquals($id3, $id2);
        
        $this->assertEquals(5, count($id3));
        
    }
    public function test_crossover_can_be_turned_off() {
        $g = new Genome(5);
        $g->setCrossoverRate(0.0);
        
        $offspring = $g->mate(array(1,2,3,4,5), array(6,7,8,9,10));
        
        $this->assertEquals(array(1,2,3,4,5), $offspring);
    }
    
    public function test_genome_can_evaluate_fitness_of_an_individual() {
        $g = new Genome(5, 5, 10);
        $id = $g->createIndividual();
        $g->evaluate($id);
        
    }
    
    public function test_an_evaluation_function_can_be_assigned() {
        $g = new Genome(5, 5, 10);
        $g->setEvaluator("example_fitness");
        $id = $g->createIndividual();
        $this->assertEquals(7+9+6+5+10, $g->evaluate($id), print_r($id, true));
    }
    
    public function test_genome_can_return_elite_members_of_a_population() {
        $g = new Genome(2, 0, 20);
        $g->setEvaluator("example_fitness");
        $raw_population = array(
            array(7,8),
            array(1,1),
            array(2,2),
            array(3,3),
            array(4,4),
            array(9,9)
        );
        
        $elite = $g->getElite($raw_population, 2);
        $this->assertTrue(in_array(array(9,9), $elite));
        $this->assertTrue(in_array(array(7,8), $elite));
        $this->assertEquals(2, count($elite));
    }
    
    public function test_elite_members_count_cannot_exceed_population_size() {
        $g = new Genome(2, 0, 20);
        $g->setEvaluator("example_fitness");
        $raw_population = array(
            array(7,8),
            array(1,1),
            array(9,9)
        );
        
        $elite = $g->getElite($raw_population, 4);
        $this->assertEquals(3, count($elite));
    }
    
    public function test_random_seed_test() {
        $this->assertEquals(0, rand(0,5));
    }
}
